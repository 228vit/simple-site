<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('ruble', array($this, 'priceFilter')),
            new TwigFilter('ru_int', array($this, 'ruInteger')),
            new TwigFilter('ireplace', array($this, 'ireplace')),
            new TwigFilter('highlight_search', array($this, 'highlight_search')),
        );
    }

    public function priceFilter($number, $decimals = 2, $decPoint = ',', $thousandsSep = ' ')
    {
        return number_format($number, $decimals, $decPoint, $thousandsSep);
    }

    public function ruInteger($number, $decimals = 0, $decPoint = ',', $thousandsSep = ' ')
    {
        return number_format($number, $decimals, $decPoint, $thousandsSep);
    }

    public function ireplace($string, $search, $replace)
    {
        return str_ireplace($search, $replace, $string);
    }

    public function highlight_search($string, $search)
    {
        if (empty($search)) {
            return $string;
        }

        $pattern = "/($search)/i";
        $replacement = '<strong>$1</strong>';

        return preg_replace($pattern, $replacement, $string);
    }
}
