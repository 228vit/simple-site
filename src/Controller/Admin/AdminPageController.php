<?php

namespace App\Controller\Admin;

use App\Entity\Page;
use App\Filter\PageFilter;
use App\Repository\PageRepository;
use Doctrine\ORM\Query;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Page controller.
 */
class AdminPageController extends Controller
{
    use AdminTraitController;

    CONST ROWS_PER_PAGE = 10;
    CONST MODEL = 'page';

    /**
     * Lists all page entities.
     *
     * @Route("admin/page/index", name="admin_page_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, SessionInterface $session)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var PageRepository $repository */
        $repository = $em->getRepository('App:Page');

        $filter_form = $this->createForm(PageFilter::class, null, array(
            'action' => $this->generateUrl('admin_apply_filter', ['model' => self::MODEL]),
            'method' => 'POST',
        ));

        /** @var Query $query */
        $query = $this->buildQuery($repository, $request, $session, $filter_form, self::MODEL);

        /** @var Paginator $paginator */
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            self::ROWS_PER_PAGE  /*limit per page*/
        );

        $pages = $query->getResult();

        return $this->render('admin/page/index_new.html.twig', array(
            'pages' => $pages,
            'pagination' => $pagination,
            'current_filters' => $this->current_filters,
            'filter_form' => $filter_form->createView(),
            'model' => self::MODEL,
        ));
    }

    /**
     * Creates a new page entity.
     *
     * @Route("admin/page/new", name="admin_page_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, ValidatorInterface $validator)
    {
        $page = new Page();
        $form = $this->createForm('App\Form\PageType', $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($page);
            $em->flush($page);
            $this->addFlash('success', 'New record was created!');

            return $this->redirectToRoute('admin_page_edit', array('id' => $page->getId()));
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due creating object!');
        }

        return $this->render('admin/page/new.html.twig', array(
            'page' => $page,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a page entity.
     *
     * @Route("admin/page/{id}", name="admin_page_show")
     * @Method("GET")
     */
    public function showAction(Page $page)
    {
        $deleteForm = $this->createDeleteForm($page);

        return $this->render('admin/page/show.html.twig', array(
            'page' => $page,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing page entity.
     *
     * @Route("admin/page/{id}/edit", name="admin_page_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Page $page)
    {
        $deleteForm = $this->createDeleteForm($page);
        $editForm = $this->createForm('App\Form\PageType', $page);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Your changes were saved!');

            return $this->redirectToRoute('admin_page_edit', array('id' => $page->getId()));
        }
        if ($editForm->isSubmitted() && !$editForm->isValid()) {
            $this->addFlash('danger', 'Errors due saving object!');
        }

        return $this->render('admin/page/edit.html.twig', array(
            'page' => $page,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'model' => self::MODEL,
        ));
    }

    /**
     * Deletes a page entity.
     *
     * @Route("admin/page/{id}", name="admin_page_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Page $page)
    {
        $filter_form = $this->createDeleteForm($page);
        $filter_form->handleRequest($request);

        if ($filter_form->isSubmitted() && $filter_form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($page);
            $em->flush($page);

            $this->addFlash('success', 'Record was successfully deleted!');
        }

        if (!$filter_form->isValid()) {
            /** @var FormErrorIterator $errors */
            $errors = $filter_form->getErrors()->__toString();
            $this->addFlash('danger', 'Error due deletion! ' . $errors);
        }

        return $this->redirectToRoute('admin_page_index');
    }

    /**
     * Creates a form to delete a page entity.
     *
     * @param Page $page The page entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Page $page)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_page_delete', array('id' => $page->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }



    /**
     * Lists all page entities.
     *
     * @Route("admin/page/index_orig", name="admin_page_index_orig")
     * @Method("GET")
     */
    public function indexOrigAction(Request $request, SessionInterface $session)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var PageRepository $repository */
        $repository = $em->getRepository('App:Page');

        $sort_by = $request->query->get('sort_by', 'id');
        $order = $request->query->get('order', 'asc');

        // todo: replace to POST
        // todo: save filter to session
        // we store different filters by different platforms
        $session_filters = $session->get('filters', array());

        $filter_form = $this->createForm(PageFilter::class, null, array(
            'action' => $this->generateUrl('admin_apply_filter', ['model' => 'page']),
            'method' => 'POST',
        ));

        if (count($session_filters) && isset($session_filters[$this->filter_session_name])) {
            $current_filters = $session_filters[$this->filter_session_name];
            $filter_form->submit($current_filters);

            $filterBuilder = $repository->createQueryBuilder('page');
            $this->get('lexik_form_filter.query_builder_updater')
                ->addFilterConditions($filter_form, $filterBuilder)
                ->orderBy('page.'.$sort_by, $order)
            ;

            $query = $filterBuilder->getQuery();
        } else {
            $current_filters = null;
            // default query w/sorting
            $query = $repository->createQueryBuilder('page')
                ->orderBy('page.'.$sort_by, $order)
                ->getQuery();
        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            self::ROWS_PER_PAGE  /*limit per page*/
        );

        $pages = $query->getResult();

        return $this->render('admin/page/index_new.html.twig', array(
            'pages' => $pages,
            'pagination' => $pagination,
            'current_filters' => $current_filters,
            'filter_form' => $filter_form->createView(),
        ));
    }

}
