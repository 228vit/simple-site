<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use App\Filter\ProductFilter;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Doctrine\ORM\Query;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Service\FileUploader;

class AdminProductController extends Controller
{
    use AdminTraitController;

    CONST ROWS_PER_PAGE = 10;
    CONST MODEL = 'product';
    CONST ENTITY_NAME = 'Product';
    CONST NS_ENTITY_NAME = 'App:Product';

    /**
     * Lists all product entities.
     *
     * @Route("admin/product/index", name="admin_product_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, SessionInterface $session)
    {
        $pagination = $this->getPagination($request, $session, ProductFilter::class);

        return $this->render('admin/product/index.html.twig', array(
            'pagination' => $pagination,
            'current_filters' => $this->current_filters,
            'filter_form' => $this->filter_form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
            'list_fields' => [
                'a.id' => [
                    'title' => 'ID',
                    'row_field' => 'id',
                    'sorting_field' => 'product.id',
                    'sortable' => true,
                ],
                'a.name' => [
                    'title' => 'Name',
                    'row_field' => 'name',
                    'sorting_field' => 'product.name',
                    'sortable' => true,
                ],
                'a.price' => [
                    'title' => 'Price',
                    'row_field' => 'price',
                    'sorting_field' => 'product.price',
                    'sortable' => true,
                ],
                'a.pic' => [
                    'title' => 'Pic',
                    'row_field' => 'pic',
                    'sorting_field' => 'product.pic',
                    'sortable' => false,
                ],
                'a.isActive' => [
                    'title' => 'Is active?',
                    'row_field' => 'isActive',
                    'sorting_field' => 'product.isActive',
                    'sortable' => false,
                ],
            ]
        ));
    }


    /**
     * Creates a new product entity.
     *
     * @Route("admin/product/new", name="admin_product_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, ValidatorInterface $validator, FileUploader $fileUploader)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPERADMIN', null, 'Unable to access this page!');

        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $product->getPicFile();

            if (null !== $file) {
                $fileName = $fileUploader->upload($file);
                $product->setPic($fileName);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush($product);

            $this->addFlash('success', 'New record was created!');

            return $this->redirectToRoute('admin_product_edit', array('id' => $product->getId()));
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due creating object!');
        }

        return $this->render('admin/common/new.html.twig', array(
            'product' => $product,
            'form' => $form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,

        ));
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("admin/product/{id}/edit", name="admin_product_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Product $product, FileUploader $fileUploader)
    {
        $deleteForm = $this->createDeleteForm($product);
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /*
             * 1. Если в форме оставить только pic, то при отправке формы без фото, поле pic стирается
             * 2. Если добавить picFile, которые не хранится в базе, то получается что при изменении только
             * фото, модель не считается изменённой, и вообще не сохраняется, не срабатывают Listeners
             * как вариант, в таком случае менять UpdatedAt, и дальше пусть работает Listener
             */
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $product->getPicFile();

            if (null !== $file) {
                $fileName = $fileUploader->upload($file);
                $product->setPic($fileName);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush($product);

            $this->addFlash('success', 'Your changes were saved!');
//            return $this->redirectToRoute('admin_product_edit', array('id' => $product->getId()));
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due saving object!');
        }

        return $this->render('admin/product/edit.html.twig', array(
            'row' => $product,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
        ));
    }


    /**
     * Finds and displays a product entity.
     *
     * @Route("admin/product/{id}", name="admin_product_show")
     * @Method("GET")
     */
    public function showAction(Product $product)
    {
        $deleteForm = $this->createDeleteForm($product);

        return $this->render('admin/product/show.html.twig', array(
            'product' => $product,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a product entity.
     *
     * @Route("admin/product/{id}", name="admin_product_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Product $product)
    {
        $filter_form = $this->createDeleteForm($product);
        $filter_form->handleRequest($request);

        if ($filter_form->isSubmitted() && $filter_form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush($product);

            $this->addFlash('success', 'Record was successfully deleted!');
        }

        if (!$filter_form->isValid()) {
            /** @var FormErrorIterator $errors */
            $errors = $filter_form->getErrors()->__toString();
            $this->addFlash('danger', 'Error due deletion! ' . $errors);
        }

        return $this->redirectToRoute('admin_product_index');
    }

    /**
     * Creates a form to delete a product entity.
     *
     * @param Product $product The product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Product $product)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_product_delete', array('id' => $product->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

}
