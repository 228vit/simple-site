<?php

namespace App\Controller\Admin;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Knp\Component\Pager\Paginator;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

trait AdminTraitController
{

    private $current_filters = null;
    private $filter_form;

    private function getPagination(Request $request, SessionInterface $session, $filter_form_class)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var EntityRepository $repository */
        $repository = $em->getRepository(self::NS_ENTITY_NAME);

        $this->filter_form = $this->createForm($filter_form_class, null, array(
            'action' => $this->generateUrl('admin_apply_filter', ['model' => self::MODEL]),
            'method' => 'POST',
        ));


        /** @var Query $query */
        $query = $this->buildQuery($repository, $request, $session, $this->filter_form, self::MODEL);

        /** @var Paginator $paginator */
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            self::ROWS_PER_PAGE  /*limit per page*/
        );

        return $pagination;
    }

    private function buildQuery(EntityRepository $repository, Request $request,
        SessionInterface $session, FormInterface $filter_form, string $model)
    {
        $sort_by = $request->query->get('sort_by', 'id');
        $order = $request->query->get('order', 'asc');
        $session_filters = $session->get('filters', array());

        if (count($session_filters) && isset($session_filters[$model])) {
            $this->current_filters = $session_filters[$model];
            $filter_form->submit($this->current_filters);

            $filterBuilder = $repository->createQueryBuilder($model);
            
            $this->get('lexik_form_filter.query_builder_updater')
                ->addFilterConditions($filter_form, $filterBuilder)
                ->orderBy($model.'.'.$sort_by, $order)
            ;

            $query = $filterBuilder->getQuery();
        } else {
            $this->current_filters = null;
            // default query w/sorting
            $query = $repository->createQueryBuilder($model)
                ->orderBy($model.'.'.$sort_by, $order)
                ->getQuery();
        }

        return $query;
    }

    /**
     * Save filter values in session.
     *
     * @Route("admin/apply/filter/{model}", name="admin_apply_filter")
     * @Method("POST")
     */
    public function saveModelFilter(Request $request, SessionInterface $session, $model)
    {
        $filter = $request->request->get('item_filter');
        // todo: validate?
        // unset empty values
        if (is_array($filter)) {
            $filter = array_filter($filter, function($value) { return $value !== ''; });
            // csrf???
            unset($filter['_token']);
        }

        // save filter to session
        $session->set('filters', array(
            $model => $filter,
        ));

        // redirect to referer
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("admin/reset/filter/{model}", name="admin_reset_filter")
     */
    public function resetModelFilter(Request $request, SessionInterface $session, $model)
    {
        $current_filters = $session->get('filters', false);
        if (isset($current_filters[$model])) {
            unset($current_filters[$model]);
        }

        $session->set('filters', $current_filters);

        // redirect to referer
        return $this->redirect($request->headers->get('referer'));
    }



//    /**
//     * Creates a new page entity.
//     *
//     * @Route("admin/{model}/new", name="admin_entity_new")
//     * @Method({"GET", "POST"})
//     */
//    public function newAction(Request $request, $model, string $entity_class_name)
//    {
//        $entity = new $entity_class_name;
//        $form = $this->createForm($entity_class_name, $entity); // 'App\Form\PageType'
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//
//            $em = $this->getDoctrine()->getManager();
//
//            $em->persist($entity);
//            $em->flush($entity);
//            $this->addFlash('success', 'New record was created!');
//
//            return $this->redirectToRoute('admin_entity_edit', array('id' => $entity->getId()));
//        }
//        if ($form->isSubmitted() && !$form->isValid()) {
//            $this->addFlash('danger', 'Errors due creating object!');
//        }
//
//        return $this->render('admin/entity/new.html.twig', array(
//            'entity' => $entity,
//            'model' => $model,
//            'form' => $form->createView(),
//        ));
//    }
}