require('../css/app.scss');

// loads the jquery package from node_modules
let $ = require('jquery');
window.Popper = require('popper.js');
require('bootstrap');
require('@fancyapps/fancybox');
require('startbootstrap-sb-admin/js/sb-admin.min.js');

// import the function from greet.js (the .js extension is optional)
// ./ (or ../) means to look for a local file
let greet = require('./greet');

$(document).ready(function() {
    $.fancybox.open($('.fancybox-me'));

    $('.delete-button').on('click', function () {
        return confirm('Are you sure???')
    })

    $('#apply_filter').on('click', function () {
        // alert('apply filer!' + $("form[name='item_filter']").attr('action'));
        $("#item_filter").submit();
    })

    $('form#submit_create_form').on('click', function () {
        $('#create_form').submit();
    })

    // $('.datepicker').datepicker({
    //     format: 'yyyy-mm-dd',
    //     autoclose: true,
    //     weekStart: 1
    // });
});